function [ sortedImages ] = jigsaw( images )
%Preallocation
sortedImages = cell(1,1);
frames = cell(1,size(images,2));
descriptors = cell(1,size(images,2));
matches = cell(size(images,2),size(images,2));
frameKeypoints = cell(size(images,2),size(images,2));
leftRightMatches = cell(size(images,2),size(images,2));
topBottomMatches = cell(size(images,2),size(images,2));
TL = zeros(1, size(images,2));
TR = zeros(1, size(images,2));
BL = zeros(1, size(images,2));
BR = zeros(1, size(images,2));
leftAmount = zeros(1, size(images,2));
topAmount = zeros(1, size(images,2));
rightAmount = zeros(1, size(images,2));

if (size(images{1,1}, 3) == 3)
    for i=1:size(images,2)
        %finding sift descriptors for each image
        [frames{1,i}, descriptors{1,i}] = vl_sift(single(rgb2gray(images{1,i})));
    end
else    
    for i=1:size(images,2)
        %finding sift descriptors for each image
        [frames{1,i}, descriptors{1,i}] = vl_sift(single(images{1,i}));
    end
end

for i=1:size(images,2)
    for k=1:size(images,2)
        if i == k
            %dont compare same image descriptors
            matches{i,k} = 0;
            frameKeypoints{i,k} = 0;
        else
            %find matching keypoints against each other image
            [matches{i,k}] = vl_ubcmatch(descriptors{1,i},descriptors{1,k});
            frameKeypoints{i,k} = [frames{1,i}(1,matches{i,k}(1,:)); frames{1,i}(2, matches{i,k}(1,:))];
        end
    end
end

%Getting each images total matches against every other image in each
%quarter
for i=1:size(images,2)
    %Getting half the width and height so quarter locations are known
    halfImageWidth = size(images{1,i},2)/2;
    halfImageHeight = size(images{1,i},1)/2;
    for k=1:size(images,2)
        if i == k
            leftRightMatches{i,k} = 0;
            topBottomMatches{i,k} = 0;
        else
            %preallocation
            leftRightMatches{i,k}(1,1) = 0;
            leftRightMatches{i,k}(1,2) = 0;
            topBottomMatches{i,k}(1,1) = 0;
            topBottomMatches{i,k}(1,2) = 0;
            for j=1:size(frameKeypoints{i,k},2)
                %Top left keypoints
                if(frameKeypoints{i,k}(1,j) < halfImageWidth && frameKeypoints{i,k}(2,j) < halfImageHeight)
                    leftRightMatches{i,k}(1,1) = leftRightMatches{i,k}(1,1) + 1;
                    topBottomMatches{i,k}(1,1) = topBottomMatches{i,k}(1,1) + 1;
                    TL(1,i) = TL(1,i)+1;
                    
                    %Top right keypoints
                elseif (frameKeypoints{i,k}(1,j) > halfImageWidth && frameKeypoints{i,k}(2,j) < halfImageHeight)
                    leftRightMatches{i,k}(1,2) = leftRightMatches{i,k}(1,2) + 1;
                    topBottomMatches{i,k}(1,1) = topBottomMatches{i,k}(1,1) + 1;
                    TR(1,i) = TR(1,i)+1;
                    
                    %Bottom left keypoints
                elseif (frameKeypoints{i,k}(1,j) < halfImageWidth && frameKeypoints{i,k}(2,j) > halfImageHeight)
                    leftRightMatches{i,k}(1,1) = leftRightMatches{i,k}(1,1) + 1;
                    topBottomMatches{i,k}(1,2) = topBottomMatches{i,k}(1,2) + 1;
                    BL(1,i) = BL(1,i)+1;
                    
                    %Bottom right keypoints
                else
                    leftRightMatches{i,k}(1,2) = leftRightMatches{i,k}(1,2) + 1;
                    topBottomMatches{i,k}(1,2) = topBottomMatches{i,k}(1,2) + 1;
                    BR(1,i) = BR(1,i)+1;
                end
            end
            leftAmount(1,i) = leftAmount(1,i) + leftRightMatches{i,k}(1,1);
            topAmount(1,i) = topAmount(1,i) + topBottomMatches{i,k}(1,1);
        end
    end
end

%Getting the index of the image with the least matches in each corner. so
%each corner is is known.
%example - TLIndex is the index of the image in the top left corner.
[~, TLIndex] = min(TL(:));
[~, TRIndex] = min(TR(:));
[~, BLIndex] = min(BL(:));
[~, BRIndex] = min(BR(:));

%This image is going to be the top left
sortedImages{1,1} = images{1,TLIndex};
sortedImageIndex(1,1) = TLIndex;

%If Top left and bottom left index are equal, the image is a panorama. Not
%a 2d image
if(TLIndex == BLIndex)
    leftMostIndex = TLIndex;
    %We now have the left most image,
    for i=1:size(images,2)-1
        for k=1:size(images,2)
            if leftMostIndex == k
                rightAmount(1,k) = -1;
            else
                rightAmount(1,k) = leftRightMatches{leftMostIndex,k}(1,2);
            end
        end
        %Getting the next right image
        [~, leftMostIndex] = max(rightAmount(:));
        sortedImages{1,i+1} = images{1,leftMostIndex};
    end
    
    %If it is a 2d image not a panorama
else
    %The image is not a panorama so the bottom left image must be
    %discovered.
    foundBL = false;
    topIndex = TLIndex;
    location = 2;
    %while the bottom left image is unknown
    while foundBL == false
        for i=1:size(images,2)
            if topIndex == i
                bottomAmount(1,i) = -1;
            else
                bottomAmount(1,i) = topBottomMatches{topIndex,i}(1,2);
            end
        end
        %Getting the next image below
        [~, topIndex] = max(bottomAmount(:));
        sortedImages{location,1} = images{1,topIndex};
        sortedImageIndex(location,1) = topIndex;
        
        %if the image below is equal to the image known as the bottom left
        %corner end while loop
        if(topIndex == BLIndex)
            foundBL = true;
        else
            location = location+1;
        end
    end
    %once the height of the image is known the width can be calculated
    finalImWidth = size(images,2)/size(sortedImages,1);
    
    %filling in the remaining image locations
    for i=1:size(sortedImages,1)
        leftMostIndex = sortedImageIndex(i,1);
        %first column is known so starting from second element
        for k=2:finalImWidth
            if (i == 1 && k == finalImWidth)
                sortedImages{i,k} = images{1,TRIndex};
                sortedImageIndex(i,k) = TRIndex;
            elseif (i == size(sortedImages,1)&& k == finalImWidth)
                sortedImages{i,k} = images{1,BRIndex};
                sortedImageIndex(i,k) = BRIndex;
            else
                for j=1:size(images,2)
                    if leftMostIndex == j
                        rightAmount(1,j) = -1;
                    else
                        rightAmount(1,j) = leftRightMatches{leftMostIndex,j}(1,2);
                    end
                end
                %Getting the next right image
                [~, leftMostIndex] = max(rightAmount(:));
                sortedImages{i,k} = images{1,leftMostIndex};
                sortedImageIndex(i,k) = leftMostIndex;
            end
        end
    end
end
end
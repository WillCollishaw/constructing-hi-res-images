function [ images, upperPadding, roundedStitchCoords ] = stitchAlignment( images, stitchTriangle, finalWarps)

rightmostColumn = zeros(1,size(images,2)-1);
roundedStitchCoords = cell(1,(size(images,2)-1)*2);
upperPadding = zeros(1,size(images, 2));

%The image will be stitched at this coordinate
for i=1:size(images,2)-1
    [~,rightmostColumn(1,i)] = max(stitchTriangle{1,i*2}(1,:));
    %Finding the new location of the stitching coordinates in the second
    %triangle because location will have changed in the warp
    roundedStitchCoords{1,(i*2)-1}  = round(stitchTriangle{1,(i*2)-1}(:,rightmostColumn(1,i))');
    roundedStitchCoords{1,i*2} = round(transformPointsForward(finalWarps(1,i),stitchTriangle{1,(i*2)}(:,rightmostColumn(1,i))'));
end
for i=1:size(images,2)-1
    %Determining which image needs padding and applying the padding to the correct image
    if(roundedStitchCoords{1,(i*2)-1}(1,2) > roundedStitchCoords{1,i*2}(1,2))
        difference = roundedStitchCoords{1,(i*2)-1}(1,2) - roundedStitchCoords{1,i*2}(1,2);
        images{1,i+1} = padarray(images{1,i+1},difference,'pre');
        upperPadding(1,i+1) = difference;
        roundedStitchCoords{1,i*2}(1,2) =  roundedStitchCoords{1,i*2}(1,2) + difference;
        if i < size(images,2)-1
            roundedStitchCoords{1,(i*2)+1}(1,2) =  roundedStitchCoords{1,(i*2)+1}(1,2) + (difference);
        end
    elseif(roundedStitchCoords{1,i*2}(1,2) > roundedStitchCoords{1,(i*2)-1}(1,2))
        difference = roundedStitchCoords{1,i*2}(1,2) - roundedStitchCoords{1,(i*2)-1}(1,2);
        images{1,i} = padarray(images{1,i},difference,'pre');
        upperPadding(1,i) = difference;
        roundedStitchCoords{1,(i*2)-1}(1,2) = roundedStitchCoords{1,(i*2)-1}(1,2) + difference;
        if i > 1
            %if a previously aligned image needs to be pushed down, all
            %previous images must also be pushed down
            for j=1:i-1
                images{1,j} = padarray(images{1,j},difference,'pre');
                upperPadding(1,j) = upperPadding(1,j)+difference;
                roundedStitchCoords{1,(j*2)-1}(1,2) = roundedStitchCoords{1,(j*2)-1}(1,2) + difference;
            end
        end
        
    end
end
end
function [frameKeypoints1,frameKeypoints2] = removeOutliers(frameKeypoints1,frameKeypoints2)

%RANSAC to remove outlier keypoints
[~,inliersIndex] = estimateFundamentalMatrix(frameKeypoints1',frameKeypoints2','Method', 'RANSAC', 'NumTrials', 2000);
t1 = [];
t2 = [];
%Creating the new array only including inliers
for x = 1:size(inliersIndex,1)
    if(inliersIndex(x,1) == true)
        t1(:,size(t1,2)+1) = frameKeypoints1(:,x);
        t2(:,size(t2,2)+1) = frameKeypoints2(:,x);
    end
end
frameKeypoints1 = t1;
frameKeypoints2 = t2;
end
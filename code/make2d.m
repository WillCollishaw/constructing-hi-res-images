function finalImage = make2d(images, jig)
% MAKE2D  takes a cell array of images and stitches them together.
%   FINALIMAGE = MAKE2D(IMAGES) combines the input images to a single image,
%   with no jigsaw solver
%   FINALIMAGE = MAKE2D(IMAGES,JIG) combines the input images to a single
%   image, if jig is a logical. If JIG is true the jigsaw solver algorithm
%   will be run.

%Input error handling
if nargin < 1
    error('There are no inputs');
elseif nargin == 1
    if iscell(images)
        jig = false;
    else
        error('images must be a cell array');
    end
elseif nargin == 2
    if iscell(images) == false || islogical(jig) == false
        error('images must be a cell array and jig must be boolean');
    end
end

%setting up vlfeat library so the SIFT algorithms can be used
 run vlfeat\toolbox\vl_setup.m

%Only run jigsaw solver on demand
if(jig == true)
    images = jigsaw(images);
end

% Preallocating
stitchTriangle = cell(1,(size(images,2)-1)*2);
panoImages = cell(1,size(images,1));
originalDimensions = cell(size(images));

%Looping through vertical images
for z=1:size(images,1)
    for i=1:size(images,2)
        originalDimensions{1,i} = size(images{z,i});
    end
    %Looping through horizontal images
    for i=1:size(images,2)-1
        %% 1 - Running SIFT algorithm
        [frameKeypoints1, frameKeypoints2] = SIFT(images{z,i},images{z,i+1});
        
        %% 2 - Removing outliers using RANSAC
        [frameKeypoints1, frameKeypoints2] = removeOutliers(frameKeypoints1, frameKeypoints2);
        
        %% 3 - Warp
        %generating random variables that will be used to make triangles later on
        triangles = randperm(size(frameKeypoints1, 2), floor(size(frameKeypoints1 ,2)/3)*3);
        
        %Getting the coordinate values for the randomly generated triangles
        warpTriangle1 = frameKeypoints1(:,triangles(1,:));
        warpTriangle2 = frameKeypoints2(:,triangles(1,:));
        
        %Clearing possibly large variable from memory
        clearvars frameKeypoints1 frameKeypoints2;
        %Finding the best warp for the pair of images using RANSAC
        [finalWarps(1,i), stitchTriangle{1,i*2-1}, stitchTriangle{1,i*2}] = findBestWarp(warpTriangle1,warpTriangle2,triangles);
        
        %Clearing possibly large variable from memory
        clearvars triangles warpTriangle1 warpTriangle2;
        try
            %applying the image warp
            images{z,i+1} = imwarp(images{z,i+1},finalWarps(1,i),'OutputView', imref2d(size(images{z,i})));
        catch
            error('Error applying warp to image');
        end
    end
    
    %% 4 - Stitching warped images
    %Stitching will use the rightmost coordinate from the median warp. This
    %check determines if any triangles errored before the index of the median
    %warp. To make sure the correct warp index is found.
    [images(z,:), upperPadding, roundedStitchCoords] = stitchAlignment( images(z,:), stitchTriangle, finalWarps);
    
    [panoImages{1,z}, lowerPadding] = performStitch( images(z,:), roundedStitchCoords );
    %Clearing unused variables from memory
    clearvars  stitchTriangle;
    %% 5 - Cropping
    [panoImages{1,z}] = crop(images(z,:),panoImages{1,z}, finalWarps, upperPadding, lowerPadding, roundedStitchCoords, originalDimensions);
end

%Clearing unused variables from memory
clearvars images;
%If only a panorama return the final image.
if size(panoImages,2)== 1
    finalImage = panoImages{1,1};
    figure
    imshow(finalImage)
else
    %rotating the images so the same functions that were previously used
    %can be reused
    for i=1:size(panoImages,2)
        panoImages{1,i} = imrotate(panoImages{1,i},90);
    end
    originalDimensions = cell(size(panoImages));
    %Looping through vertical images
    
    for i=1:size(panoImages,2)
        originalDimensions{1,i} = size(panoImages{1,i});
    end
    %Repeating previous functions
    for i=1:size(panoImages,2)-1
        %% 1 - Running SIFT algorithm
        [frameKeypoints1, frameKeypoints2] = SIFT(panoImages{1,i},panoImages{1,i+1});
        
        %% 2 - Removing outliers using RANSAC
        % [frameKeypoints1, frameKeypoints2] = removeOutliers(frameKeypoints1, frameKeypoints2);
        %% 3 - Warp
        %generating random variables that will be used to make triangles later on
        
        triangles = randperm(size(frameKeypoints1, 2), floor(size(frameKeypoints1 ,2)/3)*3);
        %Getting the coordinate values for the randomly generated triangles
        warpTriangle1 = frameKeypoints1(:,triangles(1,:));
        warpTriangle2 = frameKeypoints2(:,triangles(1,:));
        
        %Clearing possibly large variable from memory
        clearvars frameKeypoints1 frameKeypoints2;
        %Finding the best warp for the pair of images using RANSAC
        [finalWarps(1,i), stitchTriangle{1,i*2-1}, stitchTriangle{1,i*2}] = findBestWarp(warpTriangle1,warpTriangle2,triangles);
        
        %Clearing possibly large variable from memory
        clearvars triangles warpTriangle1 warpTriangle2;
        try
            panoImages{1,i+1} = imwarp(panoImages{1,i+1},finalWarps(1,i),'OutputView', imref2d(size(panoImages{1,i})));
        catch
            error('Error applying warp to image');
        end
    end
    
    %% 4 - Stitching warped images
    %Stitching will use the rightmost coordinate from the median warp. This
    %check determines if any triangles errored before the index of the median
    %warp. To make sure the correct warp index is found.   
    [panoImages, upperPadding, roundedStitchCoords] = stitchAlignment( panoImages, stitchTriangle, finalWarps);
    
    [finalImage, lowerPadding] = performStitch( panoImages, roundedStitchCoords );
    %Clearing unused variables from memory
    clearvars  stitchTriangle;
    %% 5 - Cropping
    [finalImage] = crop(panoImages,finalImage, finalWarps, upperPadding, lowerPadding, roundedStitchCoords, originalDimensions);
    
    finalImage = imrotate(finalImage,270);
    figure
    imshow(finalImage)
end
end
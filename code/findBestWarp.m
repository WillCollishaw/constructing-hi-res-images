function [ finalWarp, stitchTri1, stitchTri2] = findBestWarp( warpTriangle1, warpTriangle2, triangles )

collinearPoints = zeros(1, size(triangles, 2)/3);
firstRun = true;
for j=1:size(triangles, 2)/3
    %Creating seperate triangle variables because all triangles are
    %currently stored in a single matrix
    tri1 = [warpTriangle1(1, (j*3)-2),warpTriangle1(1, (j*3)-1),warpTriangle1(1, j*3);warpTriangle1(2, (j*3)-2),warpTriangle1(2, (j*3)-1),warpTriangle1(2, j*3)];
    tri2 = [warpTriangle2(1, (j*3)-2),warpTriangle2(1, (j*3)-1),warpTriangle2(1, j*3);warpTriangle2(2, (j*3)-2),warpTriangle2(2, (j*3)-1),warpTriangle2(2, j*3)];
    
    %Centering both triangles at zero
    meanTriangle1 = mean(tri1, 2);
    meanTriangle2 = mean(tri2, 2);
    
    centredTriangle1(:,1) = tri1(:,1) - meanTriangle1;
    centredTriangle1(:,2) = tri1(:,2) - meanTriangle1;
    centredTriangle1(:,3) = tri1(:,3) - meanTriangle1;
    
    centredTriangle2(:,1) = tri2(:,1) - meanTriangle2;
    centredTriangle2(:,2) = tri2(:,2) - meanTriangle2;
    centredTriangle2(:,3) = tri2(:,3) - meanTriangle2;
    
    
    %finding the correct transformation required to make the two
    %triangles match, this will fail if there are collinear points
    %within the triangle
    try
        T = fitgeotrans(centredTriangle2', centredTriangle1', 'affine');
        if firstRun
            warps(1) = T;
            firstRun = false;
        else
            %adding the warp into the next section of the array
            warps(size(warps,2)+1) = T;
        end
    catch
        disp ('Collinear points');
        collinearPoints(1,j) = 1;
    end
end

distance = zeros(1,size(warps,2));
for m=1:size(warps,2)
    for n=1:size(warps,2)
        %finding the euclidean distance between all warps to find the
        %median warp
        distance(m,n) = sqrt(sum(sum((warps(1,m).T - warps(1,n).T).^2)));
    end
end
%Finding the average for each warp distance
meanDist = mean(distance);
%Finding the index of median warp
[~,index] = min(meanDist);
finalWarp = warps(1,index);

%If any warps failed to create
if(sum(collinearPoints(1:index)) > 0)
    index = index + sum(collinearPoints(1:index));
end

%The matching coordinates used to create the best warp
stitchTri1 = warpTriangle1(:, index*3-2:index*3);
stitchTri2 = warpTriangle2(:, index*3-2:index*3);

end
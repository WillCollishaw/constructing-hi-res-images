function [ panoImage ] = crop( images, panoImage, finalWarps, upperPadding, lowerPadding, roundedStitchCoords, originalDimensions )

%preallocated variables for cropping
topLeft = zeros(2,size(images,2)-1);
bottomLeft = zeros(2,size(images,2)-1);
topRight = zeros(2,size(images,2)-1);
bottomRight = zeros(2,size(images,2)-1);
top = zeros(1,(size(images,2)-1)*2);
bottom = zeros(1,(size(images,2)-1)*2);

%Getting coordinates for the image corners after the warp is applied
for i=1:size(images,2)-1
    [topLeftColumn, topLeftRow] = transformPointsForward(finalWarps(1,i),roundedStitchCoords{1,i*2}(1,2),0);
    topLeft(:,i) = (round([topLeftColumn, topLeftRow]));
    
    [bottomLeftColumn, bottomLeftRow] = transformPointsForward(finalWarps(1,i),roundedStitchCoords{1,i*2}(1,2),originalDimensions{1,i+1}(1,1));
    bottomLeft(:,i) = (round([bottomLeftColumn, bottomLeftRow]));
    
    [topRightColumn, topRightRow] = transformPointsForward(finalWarps(1,i),originalDimensions{1,i+1}(1,2),0);
    topRight(:,i) = (round([topRightColumn, topRightRow]));
    
    [bottomRightColumn, bottomRightRow] = transformPointsForward(finalWarps(1,i),originalDimensions{1,i+1}(1,2),originalDimensions{1,i+1}(1,1));
    bottomRight(:,i) = (round([bottomRightColumn, bottomRightRow]));
    
    top(1,(i*2)-1) = topLeft(2,i);
    top(1,i*2) = topRight(2,i);
    bottom(1,(i*2)-1) = bottomLeft(2,i);
    bottom(1,i*2) = bottomRight(2,i);
end

%Calculating how much to be cropped from top, bottom and right of image
[~,maxTop] = max(top(1,:));
[~,maxBottom] = min(bottom(1,:));
right = [topRight(1,size(topRight,2)),bottomRight(1,size(bottomRight,2))];
[~,minRight] = min(right(1,:));
[~,maxUpperPadding] = max(upperPadding(1,:));
[~,maxLowerPadding] = max(lowerPadding(1,:));

if top(1,maxTop) < 0
    cropAmountTop = upperPadding(1, maxUpperPadding(1,1));
else
    cropAmountTop = top(1,maxTop) + upperPadding(1, maxUpperPadding(1,1));
end
cropAmountBottom = originalDimensions{1,i+1}(1,1) - bottom(1,maxBottom) + lowerPadding(1, maxLowerPadding(1,1));

if right(1,minRight) < originalDimensions{1,i+1}(1,2)
    cropAmountRight = originalDimensions{1,i+1}(1,2) - right(1,minRight);
    %cropping the right edge
    panoImage(:,end-cropAmountRight:end,:) = [];
end

%cropping the top
panoImage(1:cropAmountTop,:,:) = [];
%cropping the bottom
panoImage(end-cropAmountBottom:end,:,:) = [];

end


function [frameKeypoints1,frameKeypoints2] = SIFT(image1, image2)

%creating a single 2d matrix represtantion of an RGB image for SIFT if
%requied
if (size(image1, 3) == 3)
    [frames1, descriptors1] = vl_sift(single(rgb2gray(image1)));
    [frames2, descriptors2] = vl_sift(single(rgb2gray(image2)));
else
    
    [frames1, descriptors1] = vl_sift(single(image1));
    [frames2, descriptors2] = vl_sift(single(image2));
end

%Keypoint matching
[matches] = vl_ubcmatch(descriptors1,descriptors2);

%sorting the matches into a useable array
frameKeypoints1 = [frames1(1,matches(1,:)); frames1(2, matches(1,:))];
frameKeypoints2 = [frames2(1,matches(2,:)); frames2(2, matches(2,:))];

%minimum of three feature points required to run the algorithm
if size(matches,2) < 3
    error 'Not enough feature point matches found to align images'
end
end
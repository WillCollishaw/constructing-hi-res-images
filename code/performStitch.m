function [ image, lowerPadding ] = performStitch( images,roundedStitchCoords )

imageHeight = zeros(1,size(images,2));
lowerPadding = zeros(1,size(images,2));

for i=1:size(images,2)
    imageHeight(1,i) = size(images{1,i},1);
    %Removing a the bottom row if there is an uneven amount of rows helps with
    %concatenation
    if  mod(imageHeight(1,i),2) ~= 0
        images{1,i}(end,:,:) = [];
        imageHeight(1,i) = imageHeight(1,i) - 1;
    end
end

[~,maxHeight] = max(imageHeight(:));

%making all images same height for stitching by adding padding to the
%bottom of smaller images
for i=1:size(images,2)
    if i ~= maxHeight
        images{1,i} = padarray(images{1,i},imageHeight(1,maxHeight)-imageHeight(1,i),'post');
        lowerPadding(1,i) = imageHeight(1,maxHeight)-imageHeight(1,i);
    end
end

for i=1:size(images,2)
    if i == 1
        %Image matrix is created with the left image up to the stitching
        %coordinates
        image = images{1,i}(:,1:roundedStitchCoords{1,1}(1,1),:);
    elseif i == size(images,2)
        %adding remainder of the final image after stitching coordinates
        image = [image,images{1,i}(:,roundedStitchCoords{1,(i*2)-2}(1,1):end,:)];
    else
        %addng the correct amount from the current image
        image = [image,images{1,i}(:,roundedStitchCoords{1,(i*2)-2}(1,1):roundedStitchCoords{1,(i*2)-1}(1,1),:)];
    end
end
end
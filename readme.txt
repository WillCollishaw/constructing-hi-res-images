REQUIREMENTS:
MATLAB - Minimum version R2013a
MATLAB Image Processing Toolbox

make2d.m file starts the algorithm using the other .m files within

HOW TO USE IN MATLAB

1 - individually imread each selected image using a = imread('filename.jpg');
2 - store each image in a cell array - images  = {a,b,c,d};
3 - if image is two rows of four images input would be two rows in the cell array - images = {a,b,c,d;e,f,g,h};
4 - make2d(images) will combine all of the images assuming they are in the correct order
5 - make2d(images, true) will sort the location of the images using the jigsaw solver before combining

6 - to save the output use a =  make2d(images) or a =  make2d(images,true) then use imwrite(a,'filename.jpg');

vlfeat folder must be in the same directory as the matlab files for the SIFT algorithm to be accessed.


VLFEAT IS AN EXTERNAL LIBRARY
Copyright (C) 2013 Milan Sulc
Copyright (C) 2012 Daniele Perrone.
Copyright (C) 2011-13 Andrea Vedaldi.
All rights reserved.
